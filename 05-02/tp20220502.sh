#!/bin/bash

read -p "¿Qué fichero desea evaluar?" fichero

if test -f "$fichero";then

	echo "El fichero existe."

	permisosejecucion=$(find $fichero -user $USER -type f -perm -u+x)

	if [ "$permisosejecucion" == "$fichero" ];then

		echo "Usted posee permisos de ejecución"
		ls -l $fichero
		read -p "¿Desea ejecutar el fichero? Ingrese y para confirmar, cualquier otra tecla para finalizar" ejecutar
		if [ "$ejecutar" == "y" ];then
			echo "Ejecutando el fichero."
		fi

	else

		echo "Usted no posee permisos de ejecución."

		if [ -O "$fichero" ]; then

			echo "Usted es el Owner del fichero."
			read -p "¿Desea poseer permisos de ejecución? Ingrese y para confirmar, cualquier otra tecla para finalizar" deseopermisos
			if [ "$deseopermisos" == "y" ]; then
				echo "Se están otorgando los permisos."
				chmod +x $fichero
				ls -l $fichero
			fi

		else

			echo "Usted no es el owner del fichero."

		fi

	fi

else

	echo "El fichero no existe."

fi

#EOF
